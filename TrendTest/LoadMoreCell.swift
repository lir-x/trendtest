//
//  LoadMoreCell.swift
//  TrendTest
//
//  Created by Lir-x on 25.02.17.
//  Copyright © 2017 Lir-x. All rights reserved.
//

import UIKit

class LoadMoreCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var label: UILabel!

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
