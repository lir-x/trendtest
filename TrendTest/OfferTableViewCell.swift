//
//  OfferTableViewCell.swift
//  TrendTest
//
//  Created by Lir-x on 22.02.17.
//  Copyright © 2017 Lir-x. All rights reserved.
//

import UIKit

class OfferTableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    
    @IBOutlet weak var releaseDate: UILabel!
    
    @IBOutlet weak var offerImage: UIImageView!
    
    @IBOutlet weak var transportTypeImage: UIImageView!
    
    @IBOutlet weak var builder: UILabel!
    
    @IBOutlet weak var roomsLabel: UILabel!
    
    @IBOutlet weak var pricesLabel: UILabel!
    
    @IBOutlet weak var region: UILabel!

    @IBOutlet weak var nearestMetro: UILabel!
    
    @IBOutlet weak var distanceTime: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }

}
