//
//  OfferModel.swift
//  TrendTest
//
//  Created by Lir-x on 22.02.17.
//  Copyright © 2017 Lir-x. All rights reserved.
//

import Foundation

class OfferModel {
    
    
    let id: Int
    let name: String
    let deadline: String
    let advantages: [String]
    let minPrices: [Price]
    
    let guid: String
    let address: String
    let appartCount: Int
    
    let builder: Builder
    let region: Region
    let subways: [Subway]
    
    let imageLink: String
    let marked: Bool
    
    let latitude: Double
    let longitude: Double
    
    init(id: Int,
         name: String,
         deadline: String,
         guid: String,
         imageLink: String,
         prices: [Price],
         advantages: [String],
         address: String,
         appartCount: Int,
         builder: Builder,
         region: Region,
         subways: [Subway],
         marked: Bool,
         latitude: Double,
         longitude: Double) {
    
        self.id = id
        self.name = name
        self.deadline = deadline
        self.guid = guid
        self.imageLink = imageLink
        self.minPrices = prices
        self.advantages = advantages
        self.address = address
        self.appartCount = appartCount
        self.builder = builder
        self.region = region
        self.subways = subways
        self.marked = marked
        self.latitude = latitude
        self.longitude = longitude
    }
    func nearestSubway() -> Subway? {
        return self.subways.min(by: {$0.0 < $0.1})
    }
}

struct Builder {
    var id: Int
    var name: String
}
struct Region {
    var id: Int
    var isCityRegion: Int
    var alias: String
    var name: String
}
struct Subway: Comparable, Equatable {
    var distancetype: String
    var alias: String
    var name: String
    var distanceTiming: Int
    var lineNumber: Int
    
    
    static func <(lhs: Subway, rhs: Subway) -> Bool {
        if lhs.distancetype == rhs.distancetype {
            return lhs.distanceTiming < rhs.distanceTiming
        }
        if lhs.distancetype == "пешком" {
            return true
        } else {
            return false
        }
    }
    
    // MARK: Comparable
    
    static func ==(lhs: Subway, rhs: Subway) -> Bool {
        return  lhs.name == rhs.name &&
                lhs.distancetype == rhs.distancetype &&
                lhs.distanceTiming == rhs.distanceTiming
    }

}

struct Price {
    let rooms: String
    let price: Int
    let totalArea: String
}

