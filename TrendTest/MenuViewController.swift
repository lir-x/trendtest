//
//  MenuViewController.swift
//  TrendTest
//
//  Created by Lir-x on 22.02.17.
//  Copyright © 2017 Lir-x. All rights reserved.
//

import UIKit
import SDWebImage
import MKDropdownMenu


class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, MKDropdownMenuDelegate, MKDropdownMenuDataSource {
    
    var priceFrom: Int? {
        didSet{
            self.offers.removeAll()
            self.loadMoreOffers()
            if let priceTo = self.priceTo,
                let priceFrom = self.priceFrom{
                if priceTo <= priceFrom {
                    self.priceTo = priceFrom + 500_000
                }
            }
        }
    }
    var priceTo: Int? {
        didSet {
            
            self.offers.removeAll()
            self.loadMoreOffers()
        }
    }
    func loadMore() -> Int {
        if let avalible = self.avalibleOffers {
            return min(10, avalible - self.offers.count)
    }
        return 10
    }
    
    func loadMoreOffers() {

        let count = self.loadMore()
        
        self.activityIndicator.isHidden = false
        
        
        self.networkManager.fetch(count: count, offset: self.offers.count, priceFrom: self.priceFrom, priceTo: self.priceTo, success: { (data) in
            let result = self.parser.parsing(data: data)
            self.offers.append(contentsOf: result.0)
            self.avalibleOffers = result.1
            self.activityIndicator.isHidden = true
            
        }, failure: { (error) in
//            print("error: \(error)")
            
            self.activityIndicator.isHidden = true
            
            let alert = UIAlertController(title: "error", message: error.localizedDescription, preferredStyle: UIAlertControllerStyle.alert)
            let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
            alert.addAction(ok)
            self.present(alert, animated: true, completion: nil)
        })
    }
    
    
    
    let networkManager = NetworkManager.sharedInstance
    let parser = Parser()
    
    var offers: [OfferModel] = [] {
        didSet {
            tableView.reloadData()
        }
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var dropdownMenu: MKDropdownMenu!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var avalibleOffers: Int?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        setupDropdownMenu()
        
        self.view.bringSubview(toFront: self.activityIndicator)
        self.loadMoreOffers()
    }
    
    func setupDropdownMenu() {
        
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if let value = self.avalibleOffers {
            return "Доступно: \(value)"
        }
        return nil
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return offers.count + 1
    }
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == self.offers.count {
            let identifier = "LoadMore"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! LoadMoreCell
            cell.label.text = "Загрузить еще \(self.loadMore())"
            return cell
            
        } else {
            let identifier = "OfferCell"
            
            let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! OfferTableViewCell
            
            let offer = self.offers[indexPath.row]
            
            let prices = offer.minPrices.reduce("") { (total, price) -> String in
                return total+"от \(price.price)\n"
            }
            cell.pricesLabel.text = prices
            
            let rooms = offer.minPrices.reduce("", { (total, price) -> String in
                return total+"\(price.rooms) \n"
            })
            
            
            
//            cell..text = rooms
            cell.roomsLabel.text = rooms
            
            
            cell.title.text = "ЖК «\(offer.name)»"
            cell.releaseDate.text = "срок сдачи: \(offer.deadline)"
            cell.builder.text = offer.builder.name
            cell.region.text = offer.region.name
            
            if let subway = offer.nearestSubway() {
                if subway.distancetype == "пешком" {
                    cell.transportTypeImage.image = #imageLiteral(resourceName: "walkman")
                } else if subway.distancetype == "транспортом" {
                    cell.transportTypeImage.image = #imageLiteral(resourceName: "bus")
                } else {
                    cell.transportTypeImage.image = #imageLiteral(resourceName: "default-placeholder")
                }
                cell.nearestMetro.text = subway.name
                cell.distanceTime.text = "\(subway.distanceTiming) мин"
            }
            
            cell.offerImage.sd_setImage(with: URL(string: offer.imageLink), placeholderImage:#imageLiteral(resourceName: "default-placeholder.png"))
            
            return cell
        }
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == self.offers.count {
            self.loadMoreOffers()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == self.offers.count {
            return 40
        } else {
            return 220
        }
    }
    // MARK: MKDropdownMenuDataSource

        /// Return the number of column items in menu.
    
    func numberOfComponents(in dropdownMenu: MKDropdownMenu) -> Int {
        return 2
    }
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, numberOfRowsInComponent component: Int) -> Int {
        return 10
    }
    
    // MARK: MKDropdownMenuDelegate
    

    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, attributedTitleForComponent component: Int) -> NSAttributedString? {
        var str:String = ""
                if component == 0 {
                    if let price = self.priceFrom{
                        str = "от \(price) руб"
                    } else {
                        str = "от цены"
                    }
                }
                if component == 1 {
                    if let price = self.priceTo {
                        str = "до \(price) руб"
                    } else {
                        str = "до цены"
                    }
                }
        
        guard let font = UIFont(name: "Roboto-Regular", size: 14) else{
            return NSAttributedString(string: str)
        }
        let attrebute:[String: UIFont] = [NSFontAttributeName: font]
        return  NSAttributedString(string: str, attributes: attrebute)
    }
    
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, titleForRow row: Int, forComponent component: Int) -> String? {
        var value: Int
        
        switch component {
        case 0: value = 500_000
        case 1: value = self.priceFrom ?? 0
        default: value = 0
        }


        switch row {
        case 0: return "Сбросить"
        case (let r): return "\(r * 500_000 + value) руб"
        }
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, shouldUseFullRowWidthForComponent component: Int) -> Bool {
        return false
    }
    
    func dropdownMenu(_ dropdownMenu: MKDropdownMenu, didSelectRow row: Int, inComponent component: Int) {
        dropdownMenu.deselectRow(row, inComponent: component)
        
        dropdownMenu.closeAllComponents(animated: true)
        switch component {
        case 0 : self.priceFrom = self.priceValue(component: component, row: row)
        case 1 : self.priceTo = self.priceValue(component: component, row: row)
        default: break
        }
        dropdownMenu.reloadAllComponents()
    }
    
    func priceValue(component: Int, row: Int) -> Int? {
        if row == 0 {
            return nil
        }
        if component == 0 {
            return row * 500_000 + 500_000
        }
        if component == 1 {
            let value = self.priceFrom ?? 0
            return value + (row * 500_000)
        }
        return nil
    }
    
}


