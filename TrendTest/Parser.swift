//
//  Parser.swift
//  TrendTest
//
//  Created by Lir-x on 22.02.17.
//  Copyright © 2017 Lir-x. All rights reserved.
//

import Foundation
import  SwiftyJSON

class Parser {
    
    
    func parsing(data: Data) -> ([OfferModel],Int) {
        var resultArray = [OfferModel]()
        let json = JSON(data: data)

        let availableOffers = json["data"]["countResult"].intValue
        let array = json["data"]["results"].arrayValue
        for item in array {
            if let guid = item["guid"].string,
                let address = item["address"].string,
                let id = item["id"].int,
                let name = item["name"].string,
                let image = item["image"].string,
                let deadline = item["deadline"].string,
                let appartCount = item["apart_count"].int,
                let latititude = item["latitude"].double,
                let longitude = item["longitude"].double,
                let marked = item["marked"].bool,
                let subways = item["subways"].array,
                let advantages = item["advantages"].array,
                let builderDict = item["builder"].dictionary,
                let regionDict = item["region"].dictionary,
                let minPrices = item["min_prices"].array{
                
                
                let advantagesResult = advantages.map({$0.string}).flatMap({$0})
                
                var subwaysModels =  [Subway]()
                
                for sub in subways {
                    if let distanceType = sub["distance_type"].string,
                    let alias = sub["alias"].string,
                    let name = sub["name"].string,
                    let distanceTiming = sub["distance_timing"].int,
                    let lineNumber = sub["line_number"].int {
                        subwaysModels.append(Subway(distancetype: distanceType,
                                                    alias: alias,
                                                    name: name,
                                                    distanceTiming: distanceTiming,
                                                    lineNumber: lineNumber))
                        
                    }
                }
                guard let builderID = builderDict["id"]?.int,
                    let builderName = builderDict["name"]?.string else {
                        return (resultArray, availableOffers)
                }
                let builder = Builder(id: builderID, name: builderName)
                
                guard let regionID = regionDict["id"]?.int,
                let isCityRegion = regionDict["iscityregion"]?.int,
                let regionAlias = regionDict["alias"]?.string,
                    let regionName = regionDict["name"]?.string
                else {
                    return (resultArray, availableOffers)
                }
                let region = Region(id: regionID, isCityRegion: isCityRegion, alias: regionAlias, name: regionName)
                
                var prices: [Price] = []
                
                for price in minPrices {
                    if let rooms = price["rooms"].string,
                    let priceValue = price["price"].int,
                    let totalArea = price["totalArea"].string {
                        prices.append(Price(rooms: rooms, price: priceValue, totalArea: totalArea))
                    }
                }
                let offer = OfferModel(id: id,
                                       name:  name,
                                       deadline: deadline,
                                       guid: guid,
                                       imageLink: image,
                                       prices: prices,
                                       advantages: advantagesResult,
                                       address: address,
                                       appartCount: appartCount,
                                       builder: builder,
                                       region: region,
                                       subways: subwaysModels,
                                       marked: marked,
                                       latitude: latititude,
                                       longitude: longitude)
                
                resultArray.append(offer)
                
            }
                    
                    
            
        }
        return (resultArray, availableOffers)
    }
    
}
