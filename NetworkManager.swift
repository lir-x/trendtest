//
//  NetworkManager.swift
//  TrendTest
//
//  Created by Lir-x on 21.02.17.
//  Copyright © 2017 Lir-x. All rights reserved.
//

import Foundation
import Alamofire
//import  SwiftyJSON

struct Constance {
    static let link = "http://api.trend-dev.ru/v2/apartments/blocks/search/"
}
/*
 
 Принимаемые GET параметры: {
	show_type: 'list', // тип вывода
	count: 10, // кол-во показываемых ЖК на странице
	offset: 0, // смещение кол-ва показываемых объектов
	cache: false,
	price_from: 0, // Цена от
	price_to: 0 // Цена до
 }
 
 */



class NetworkManager {
    
    static let sharedInstance: NetworkManager = {
        let instance = NetworkManager()
        return instance
    }()
    
    func fetch(count: Int, offset: Int, priceFrom: Int?, priceTo: Int?, success: @escaping (Data)-> Void, failure: @escaping (Error)->Void) {
        var parameters = [
            "show_type" : "list",
            "count": "\(count)",
            "offset": "\(offset)",
            "cache" : "false",

        ]
        if let priceFrom = priceFrom {
            parameters["price_from"] = "\(priceFrom)"
        }
        if let priceTo = priceTo{
            parameters["price_to"] = "\(priceTo)"
        }
        Alamofire.request(Constance.link, parameters: parameters).responseData { (response) in
            if let error = response.error {
                failure(error)
            } else {
                if let data = response.data {
                    success(data)
                }
            }
        }
    }
    
}
